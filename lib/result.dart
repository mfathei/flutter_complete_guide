import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int totalScore;
  final Function resetQuiz;

  Result(this.totalScore, this.resetQuiz);

  String get resultPhrase {
    String resultText;

    if (totalScore <= 8) {
      resultText = 'You are good and innocent';
    } else if (totalScore <= 12) {
      resultText = 'You are likeable!';
    } else if (totalScore <= 16) {
      resultText = 'You are so bad';
    } else {
      resultText = 'You did it';
    }

    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            resultPhrase,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          FlatButton(
            child: Text(
              'Restart quiz',
              style: TextStyle(
                color: Colors.blue,
              ),
            ),
            onPressed: resetQuiz,
          )
        ],
      ),
    );
  }
}
